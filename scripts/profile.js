console.log('page load - entered main.js for js-other api');
send_button = document.getElementById("submit_button");
send_button.onmouseup = searchNameWithResponse;

function loadPokemon(){
    var name = getParameterByName('name');
    name = name[0].toUpperCase() + name.substring(1); // title case
    var id = getParameterByName('id');

    // set pokemon name
    document.title = name;
    document.getElementById("pokemon-name").innerHTML = name;

    // set image
    //document.getElementById('pri_image_url').alt = "picture of " + name;
    var json = getJSON(id);

    // set description
    console.log("setting description");
    getSpeciesURL(id)
    .then(speciesURL => setDescription(speciesURL));

    // set stats
    console.log("setting stats");
    setStats(id);

    // set type
    console.log("setting type");
    setType(id);

    // set evolutions
    //getEvolutionChain(id);
    console.log("setting evolution chain");
    setEvolutionChain(id);
}

function setType(pokemonId){
    console.log("pokemon ID: " + pokemonId);
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://pokeapi.co/api/v2/pokemon/" + pokemonId;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr
    
    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log("got response for type");
        var data = JSON.parse(xhr.responseText);

        var typeString = "";
        var i;
        for(i = 0; i < data.types.length-1; i++){
            typeString = typeString.concat(data.types[i].type.name + " | ");
        }
        typeString = typeString.concat(data.types[data.types.length-1].type.name);
        console.log("got types: " + typeString);
        
        var typeElem = document.getElementById("type-label");
        typeElem.innerHTML = typeString;
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request*/
}

function setStats(pokemonId){
    console.log("pokemon ID: " + pokemonId);
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://pokeapi.co/api/v2/pokemon/" + pokemonId;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr
    
    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log("got response for stats");
        var data = JSON.parse(xhr.responseText);
        
        var stats = data.stats;
        var i;
        for(i = 0; i < stats.length; i++){
            var bar = document.getElementById(stats[i].stat.name + "-bar");
            var value = stats[i].base_stat;
            var score = Math.min(value,100);
            bar.style="width: "+score+"%";
            bar.innerHTML = value;
        }
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request*/
}

function setDescription(speciesURL){
    console.log("species URL: " + speciesURL);
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    xhr.open("GET", speciesURL, true); // 2 - associates request attributes with xhr
    
    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log("got response for description");
        var data = JSON.parse(xhr.responseText);
        var description = "The pokémon " + data.name + ". ";
        var flavor_text = data.flavor_text_entries;
        var i;
        for(i = 0; i < flavor_text.length; i++){
            if(flavor_text[i].language.name == "en" && flavor_text[i].version.name == "red"){
                description = description.concat(flavor_text[i].flavor_text + " ");
            }
        }
        console.log(description);
        document.getElementById("description-tab").innerHTML = description;

    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request*/
}

function setEvolutionChain(id){
    
    getSpeciesURL(id)
    .then(speciesURL => getEvolutionURL(speciesURL))
    .then(evolutionURL => getSpeciesURLs(evolutionURL))
    .then(speciesURLs => getDefaultPokemonURLs(speciesURLs))
    .then(defaultPokemonURLs => displayEvolutions(defaultPokemonURLs));

}

async function getSpeciesURL(id){
    const request = await fetch("https://pokeapi.co/api/v2/pokemon/" + id);
    const data = await request.json();
    console.log(data.species.url);
    return data.species.url;
}

async function getEvolutionURL(speciesURL){
    const request = await fetch(speciesURL);
    const data = await request.json();
    console.log(data.evolution_chain.url);
    return data.evolution_chain.url;
}

async function getSpeciesURLs(evolutionURL){
    const request = await fetch(evolutionURL);
    const data = await request.json();
    chainPtr = data.chain;
    var speciesURLs = getSpeciesURLRecursive(chainPtr);
    console.log(speciesURLs);
    return speciesURLs;
}

function getSpeciesURLRecursive(chainPtr){
    console.log("chain pointer size in recursive function: " + chainPtr.evolves_to.length);
    var speciesURLs = [ chainPtr.species.url ];
    console.log("adding: " + chainPtr.species.url);

    var i;
    for(i = 0; i < chainPtr.evolves_to.length; i++){
        var result = getSpeciesURLRecursive(chainPtr.evolves_to[i]);
        console.log("result before concatenation: " + result);
        console.log("species list before concatenation" + speciesURLs);
        speciesURLs = speciesURLs.concat(result);
        console.log("species list after concatenation" + speciesURLs);
    }

    console.log("returning from recursive: " + speciesURLs);
    return speciesURLs;
}

async function getDefaultPokemonURLs(speciesURLs){
    defaultPokemonURLs = [];
    var i;
    for(i = 0; i < speciesURLs.length; i++){
        const request = await fetch(speciesURLs[i]);
        const data = await request.json();
        var varieties = data.varieties;
        var j;
        for(j = 0; j < varieties.length; j++){
            if(varieties[j].is_default){
                defaultPokemonURLs.push(varieties[j].pokemon.url);
                break;
            }
        }
    }
    console.log(defaultPokemonURLs);
    return defaultPokemonURLs;
}

async function displayEvolutions(defaultPokemonURLs){
    var evolutions_div = document.getElementById("evolutions");

    var i;
    for(i = 0; i < defaultPokemonURLs.length; i++){
        console.log("staring request to " + defaultPokemonURLs[i]);
        var requestPromise = fetch(defaultPokemonURLs[i]);
        
        console.log("creating image");
        var evolution_img = document.createElement("img");
        evolution_img.width = 100;
        evolution_img.height = 100;
        evolution_img.className = "media-object";

        console.log("creating name");
        var evolution_name = document.createElement("h5");
        evolution_name.className = "media-heading";

        console.log("finishing request")
        const request = await requestPromise;
        const data = await request.json();

        console.log("setting data to match request response")
        evolution_img.src = data.sprites.front_default;
        evolution_name.innerHTML = data.species.name;

        evolutions_div.appendChild(evolution_img);
        evolutions_div.appendChild(evolution_name);
    }
}



function getJSON(id){
    console.log('getting json for id: ' + id);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://pokeapi.co/api/v2/pokemon/" + id;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log("parsing JSON");
        var response_json = JSON.parse(xhr.responseText);
        document.getElementById("pri_image_url").src = response_json["sprites"]["front_default"];
        return response_json;
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null)
}


function searchNameWithResponse(){

    var search_text = document.getElementById("search_text").value;
    var url = "http://student04.cse.nd.edu:51037/name/";
    var reurl = url + search_text;

    var xhr = new XMLHttpRequest(); // 1 - creating request object
    xhr.open("GET", reurl, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        //console.log(xhr.responseText);
        // do something
        var response = JSON.parse(xhr.responseText);
        if(response["result"] == "error"){
            console.log("API found no pokemon, run error function");
            xhr.onerror();
        }
        else{
            var nameVar = response["name"];
            var idVar = response["id"];
            window.location.replace("profile.html?name=" + nameVar + "&id=" + idVar);
        }
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
        alert("Encountered error with search API. Redirecting home.");
        window.location.replace("index.html");
    }

    // actually make the network call
    xhr.send(null);
}

// thanks to https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
// for this query parameter parsing function
function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
