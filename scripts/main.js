send_button = document.getElementById("submit_button");
send_button.onmouseup = searchNameWithResponse;

function populate(){
    console.log('entered make nw call to my api with name ' + name);

    console.log('page load - entered populate.js for js-other api');

    var navbar = document.getElementById('bs-example-navbar-collapse-1');
    lastElem = navbar;

    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://pokeapi.co/api/v2/pokemon/?offset=20&limit=2000"
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        var response_json = JSON.parse(xhr.responseText);
        var results = response_json["results"]
        results.forEach(createListing);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.log('error with call to my API')
        console.error(xhr.statusText);
        alert("Encountered error with search API. Redirecting home.");
        window.location.replace("index.html");
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request*/

} // end of make nw call

function createListing(item, index){
    var name = item["name"];
    //console.log(name);
    var url = item["url"];
    var url_tokens = url.split("/");
    var id = 1;
    if(url_tokens.length > 2){
        id = url_tokens[url_tokens.length-2];
    }
    
    var listContainer = document.getElementById("names-list");

    //var listItem = document.createElement('li');

    // Add the item text
    //listItem.innerHTML = name;
    var anchor = document.createElement("a");
    anchor.target='_blank';
    
    var para = document.createElement("p");
    para.innerHTML = name;
    var profile_img = document.createElement("img");
    //profile_img.width = 100;
    //profile_img.height = 100;
    profile_img.className = "media-object";
    profile_img.src = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + id + ".png";
    
    anchor.appendChild(profile_img);
    anchor.appendChild(para);
    anchor.setAttribute('href', "profile.html" + "?name=" + name + "&id=" + id);
    var anchor_div = document.createElement("div");
    anchor_div.class="column";
    anchor_div.style="flex: 200px";
    anchor_div.appendChild(anchor);
    

    // Add anchor to the listElement
    listContainer.appendChild(anchor_div);
}


function searchNameWithResponse(){

    var search_text = document.getElementById("search_text").value;
    console.log("searched for:" + search_text);
    var url = "http://student04.cse.nd.edu:51037/name/";
    var reurl = url + search_text;

    var xhr = new XMLHttpRequest(); // 1 - creating request object
    console.log("querying url: " + reurl);
    xhr.open("GET", reurl, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        //console.log(xhr.responseText);
        // do something
        var response = JSON.parse(xhr.responseText);
        console.log("response: " + response);
        if(response["result"] == "error"){
            console.log("API found no pokemon, run error function");
            xhr.onerror();
        }
        else{
            var nameVar = response["name"];
            var idVar = response["id"];
            window.location.replace("profile.html?name=" + nameVar + "&id=" + idVar);
        }
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
        alert("Encountered error with search API. Redirecting home.");
        window.location.replace("index.html");
    }

    // actually make the network call
    xhr.send(null);
}
