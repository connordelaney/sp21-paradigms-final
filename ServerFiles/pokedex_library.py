import json

class _pokedex:
	def __init__(self):
		self.poke_names = dict()
		self.poke_types = dict()
		self.poke_hp = dict()

	def load_poke(self, poke_file="pokedex.json"):
		f = open(poke_file)
		resp = json.load(f)
		for entry in resp:
			id = entry["id"]
			self.poke_names[id]=entry["name"]["english"]
			self.poke_types[id]=entry["type"]
			self.poke_hp[id]=entry["base"]["HP"]
		f.close()

	def get_poke_all(self):
		return self.poke_names.keys()

	def get_poke(self,pid):
		try:
			pname = self.poke_names[pid]
			ptype = self.poke_types[pid]
			php = self.poke_hp[pid]
			pokemon = list((pname,ptype,php,pid))
		except Exception as ex:
			print(str(ex))
			pokemon = None
		return pokemon

	def set_poke(self,pid,pokemon):
		self.poke_names[pid] = pokemon[0]
		self.poke_types[pid] = pokemon[1]
		self.poke_hp[pid] = pokemon[2]

	def delete_poke(self,pid):
		del(self.poke_names[pid])
		del(self.poke_types[pid])
		del(self.poke_hp[pid])

	def get_with_type(self,type):
		pokemon = list()
		for x in self.poke_types.keys():
			for t in self.poke_types[x]:
				if t.lower() == type.lower():
					pokemon.append(x)
					break
		return pokemon

	def get_poke_name(self,name):
		pokemon = list()
		for id, n in self.poke_names.items():
			if n.lower().startswith(name.lower()):
				pokemon = list((self.poke_names[id],self.poke_types[id],self.poke_hp[id],id))
				return pokemon
		pokemon[0]='error'
		return pokemon

if __name__ == '__main__':
	pdb = _pokedex()

	pdb.load_poke()
	
	print(pdb.get_poke_all())

	pdb.set_poke(1,["Andrew",["Grass"],9999])

	print(pdb.get_poke(1))

	pdb.delete_poke(1)

	print(pdb.get_poke(1))	

	print(pdb.get_with_type("Grass"))
