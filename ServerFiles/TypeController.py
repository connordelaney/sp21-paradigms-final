import cherrypy
import re, json
from pokedex_library import _pokedex

class TypeController(object):
	def __init__(self, pdb=None):
		if pdb is None:
			self.pdb = _pokedex()
			self.pdb.load_poke()
		else:
			self.pdb = pdb

	def GET_KEY(self, type):
		output = {'result':'success'}
		type = str(type)
		output['entries'] = list()

		try:
			for x in self.pdb.get_with_type(type):
				pokemon = self.pdb.get_poke(x)
				entry = {'name':pokemon[0],'type':pokemon[1],'hp':int(pokemon[2]),'id':x}
				output['entries'].append(entry)
		except Exception as ex:
			output['result']='error'
			output['message']=str(ex)

		return json.dumps(output)		
