import unittest
import requests
import json

class TestPokeIndex(unittest.TestCase):

	SITE_URL = 'http://localhost:51037'
	print("Testing for server: " + SITE_URL)
	POKE_URL = SITE_URL + '/pokemon/'
	RESET_URL = SITE_URL + '/reset/'

	def setUp(self):
		data = {}
		r = requests.put(self.RESET_URL, json.dumps(data))

	def is_json(self, resp):
		try:
			json.loads(resp)
			return True
		except ValueError:
			return False

	def test_poke_index_get(self):
		r = requests.get(self.POKE_URL)
		self.assertTrue(self.is_json(r.content.decode()))
	
		resp = json.loads(r.content.decode())

		testpoke = {}

		for poke in resp['entries']:
			if poke['id'] == 31:
				testpoke = poke
				break

		self.assertEqual(testpoke['name'],'Nidoqueen')
		self.assertEqual(testpoke['hp'],90)

	def test_poke_index_post(self):
		m = {'name':'Treecko','type':['Grass'],'hp':40}
		r = requests.post(self.POKE_URL, data = json.dumps(m))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'],'success')
		self.assertEqual(resp['id'],152)

		r = requests.get(self.POKE_URL + str(resp['id']))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['name'], m['name'])
		self.assertEqual(resp['type'], m['type'])

	def test_poke_index_delete(self):
		m = {}
		r = requests.delete(self.POKE_URL, data = json.dumps(m))
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		if resp['result'] == 'error':
			print(resp['message'])
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.POKE_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		resp = json.loads(r.content.decode())
		entries = resp['entries']
		self.assertFalse(entries)

if __name__ == '__main__':
	unittest.main()
