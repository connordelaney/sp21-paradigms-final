import unittest
import requests
import json

class TestRatings(unittest.TestCase):

    SITE_URL = 'http://localhost:51037' # replace with your port id
    print("Testing for server: " + SITE_URL)
    TYPE_URL = SITE_URL + '/type/'
    RESET_URL = SITE_URL + '/reset/'

    def setUp(self):
        m = {}
        r = requests.put(self.RESET_URL, json.dumps(m))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_type_get_key(self):
        type = 'Ghost'

        r = requests.get(self.TYPE_URL + str(type))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(len(resp['entries']), 3)
        self.assertIn('Ghost',resp['entries'][0]['type'])

if __name__ == "__main__":
    unittest.main()
