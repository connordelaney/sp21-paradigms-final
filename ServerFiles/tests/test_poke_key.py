import unittest
import requests
import json

class TestMovies(unittest.TestCase):

	SITE_URL = 'http://localhost:51037' # replace with your port number and 
	print("testing for server: " + SITE_URL)
	POKE_URL = SITE_URL + '/pokemon/'
	RESET_URL = SITE_URL + '/reset/'

	def setUp(self):
		m = {}
		r = requests.put(self.RESET_URL, data = json.dumps(m))

	def is_json(self, resp):
		try:
			json.loads(resp)
			return True
		except ValueError:
			return False

	def test_poke_get_key(self):
		poke_id = 1
		r = requests.get(self.POKE_URL + str(poke_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['name'], 'Bulbasaur')
		self.assertEqual(resp['hp'], 45)

	def test_poke_put_key(self):
		poke_id = 1

		r = requests.get(self.POKE_URL + str(poke_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['name'], 'Bulbasaur')
		self.assertEqual(resp['type'], ['Grass','Poison'])

		m = {}
		m['name'] = 'Bulb'
		m['type'] = ['Grass']
		m['hp'] = 1
		r = requests.put(self.POKE_URL + str(poke_id), data = json.dumps(m))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.POKE_URL + str(poke_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['name'], m['name'])
		self.assertEqual(resp['type'], m['type'])

	def test_poke_delete_key(self):
		poke_id = 95

		m = {}
		r = requests.delete(self.POKE_URL + str(poke_id), data = json.dumps(m))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.POKE_URL + str(poke_id))
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		resp = json.loads(r.content.decode('utf-8'))
		self.assertEqual(resp['result'], 'error')

if __name__ == "__main__":
	unittest.main()
