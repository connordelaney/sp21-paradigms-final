import requests
import json
import sys

def print_stuff(evolution_chain):
    print(evolution_chain["species"]["name"])
    species_url = evolution_chain["species"]["url"]
    poke_num = species_url.split('/')[-2]
    species_url = 'https://pokeapi.co/api/v2/pokemon/' + poke_num
    print(f'\t{species_url}')
    response = requests.get(species_url)
    poke_dict = json.loads(response.content)
    img_url = poke_dict["sprites"]["front_default"]
    print(f'\t{img_url}')

poke_num = 1
if len(sys.argv) > 0:
    poke_num = sys.argv[1]

print('Requesting Data for Pokemon number ' + str(poke_num))

response = requests.get(f'https://pokeapi.co/api/v2/evolution-chain/{poke_num}/')
evolution_chain = json.loads(response.content)
evolution_chain = evolution_chain["chain"]
print_stuff(evolution_chain)
while len(evolution_chain["evolves_to"]) > 0:
    evolution_chain = evolution_chain["evolves_to"][0]
    print_stuff(evolution_chain)
    
