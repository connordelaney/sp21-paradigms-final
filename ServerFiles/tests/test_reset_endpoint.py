import unittest
import requests
import json

class TestReset(unittest.TestCase):

	SITE_URL = 'http://localhost:51037'
	print('Testing for server: ' + SITE_URL)
	RESET_URL = SITE_URL + '/reset/'

	def test_put_reset_index(self):
		m = {}
		r = requests.put(self.RESET_URL, json.dumps(m))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')
		r = requests.get(self.SITE_URL + '/pokemon/')
		resp = json.loads(r.content.decode())
		entries = resp['entries']
		self.assertEqual(entries[0]['name'], 'Bulbasaur')

	def test_put_reset_key(self):
		m = {}
		r = requests.put(self.RESET_URL, json.dumps(m))

		# Change Movie Title and Genre
		poke_id = 31
		m['name'] = 'Not'
		m['type'] = ['Fail']
		m['hp'] = 31
		r = requests.put(self.SITE_URL + '/pokemon/' + str(poke_id), data=json.dumps(m))

		# Reset the changed movie back to original
		m = {}
		r = requests.put(self.RESET_URL + str(poke_id), data=json.dumps(m))
		resp = json.loads(r.content.decode())	
		self.assertEqual(resp['result'], 'success')

		# Check if effective
		r = requests.get(self.SITE_URL + '/pokemon/')
		resp = json.loads(r.content.decode())	
		entries = resp['entries']
		self.assertEqual(entries[0]['name'], 'Bulbasaur')

if __name__ == '__main__':
	unittest.main()
