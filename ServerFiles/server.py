import cherrypy
from PokeController import PokeController
from ResetController import ResetController
from TypeController import TypeController
from NameController import NameController
from pokedex_library import _pokedex

def start_service():
	dispatcher = cherrypy.dispatch.RoutesDispatcher()

	pdb = _pokedex()

	pokeController = PokeController(pdb=pdb)
	resetController = ResetController(pdb=pdb)
	typeController = TypeController(pdb=pdb)
	nameController = NameController(pdb=pdb)

	dispatcher.connect('poke_get', '/pokemon/:poke_id', controller=pokeController, action = 'GET_KEY', conditions=dict(method=['GET']))
	dispatcher.connect('poke_put', '/pokemon/:poke_id', controller=pokeController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
	dispatcher.connect('poke_delete', '/pokemon/:poke_id', controller=pokeController, action = 'DELETE_KEY', conditions=dict(method=['DELETE']))
	dispatcher.connect('poke_index_get', '/pokemon/', controller=pokeController, action = 'GET_INDEX', conditions=dict(method=['GET']))
	dispatcher.connect('poke_index_post', '/pokemon/', controller=pokeController, action = 'POST_INDEX', conditions=dict(method=['POST']))
	dispatcher.connect('poke_index_delete', '/pokemon/', controller=pokeController, action = 'DELETE_INDEX', conditions=dict(method=['DELETE']))

	dispatcher.connect('reset_put', '/reset/:poke_id', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
	dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

	dispatcher.connect('type_get', '/type/:type', controller=typeController, action='GET_KEY', conditions=dict(method=['GET']))

	dispatcher.connect('name_get', '/name/:name', controller=nameController, action='GET_KEY', conditions=dict(method=['GET']))

	dispatcher.connect('poke_options', '/pokemon//', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('poke_key_options', '/pokemon/:key', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

	conf = {
		'global' : {
			'server.thread_pool': 5,
			'server.socket_host' : 'student04.cse.nd.edu',
			'server.socket_port': 51037
		},
	'/': {
		'request.dispatch' : dispatcher,
		'tools.CORS.on' : True,
		}
	}

	cherrypy.config.update(conf)
	app = cherrypy.tree.mount(None, config=conf)
	cherrypy.quickstart(app)

class optionsController:
	def OPTIONS(self, *args, **kwargs):
		return ""

# function for CORS
def CORS():
	cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
	cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
	cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"


if __name__ == '__main__':
	cherrypy.tools.CORS = cherrypy.Tool('before_finalize',CORS)
	start_service()
