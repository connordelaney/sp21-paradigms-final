import cherrypy
import re, json
from pokedex_library import _pokedex

class ResetController(object):
	def __init__(self, pdb=None):
		if pdb is None:
			self.pdb = _pokedex()
		else:
			self.pdb = pdb

	def PUT_INDEX(self):
		output = {'result':'success'}

		self.pdb.__init__()
		self.pdb.load_poke()
		
		return json.dumps(output)

	def PUT_KEY(self, poke_id):
		output = {'result':'success'}
		poke_id = int(poke_id)

		try:
			tmp = _pokedex()
			tmp.load_poke()

			pokemon = tmp.get_poke(poke_id)

			self.pdb.set_poke(poke_id,pokemon)
		except Exception as ex:
			output['result']='error'
			output['message']=str(ex)

		return json.dumps(output)
