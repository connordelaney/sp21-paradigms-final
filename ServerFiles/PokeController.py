import cherrypy
import re, json
from pokedex_library import _pokedex

class PokeController(object):
	def __init__(self, pdb=None):
		if pdb is None:
			self.pdb = _pokedex()
		else:
			self.pdb = pdb

		self.pdb.load_poke()

	def GET_KEY(self, poke_id):
		output = {'result':'success'}
		poke_id = int(poke_id)

		try:
			pokemon = self.pdb.get_poke(poke_id)
			if pokemon is not None:
				output['name']=pokemon[0]
				output['type']=pokemon[1]
				output['hp']=pokemon[2]
				output['id']=pokemon[3]
			else:
				output['result']='error'
				output['message']='Id not in pokedex'
		except Exception as ex:
			output['result']='error'
			output['message']=str(ex)

		return json.dumps(output)

	def GET_INDEX(self):
		output = {'result':'success'}
		output['entries']=[]
		
		try:
			for pid in self.pdb.get_poke_all():
				pokemon = self.pdb.get_poke(pid)
				entry = {'name':pokemon[0],'type':pokemon[1],'hp':pokemon[2],'id':pokemon[3]}
				output['entries'].append(entry)
		except Exception as ex:
			output['result']='error'
			output['message']=str(ex)
		
		return json.dumps(output)

	def PUT_KEY(self, poke_id):
		output = {'result':'success'}
		poke_id = int(poke_id)

		data = json.loads(cherrypy.request.body.read().decode('utf-8'))

		pokemon = [data['name'],data['type'],int(data['hp'])]
		self.pdb.set_poke(poke_id,pokemon)

		return json.dumps(output)

	def POST_INDEX(self):
		output = {'result':'success'}
		data = json.loads(cherrypy.request.body.read().decode('utf-8'))

		try:
			dex = sorted(list(self.pdb.get_poke_all()))
			nID = int(dex[-1])+1
			self.pdb.poke_names[nID]=data['name']
			self.pdb.poke_types[nID]=data['type']
			self.pdb.poke_hp[nID]=data['hp']
			output['id']=nID
		except Exception as ex:
			output['result']='error'
			output['message']=str(ex)

		return json.dumps(output)

	def DELETE_KEY(self,poke_id):
		output = {'result':'success'}
		poke_id=int(poke_id)

		try:
			self.pdb.delete_poke(poke_id)
		except Exception as ex:
			output['result']='error'
			output['message']=str(ex)

		return json.dumps(output)

	def DELETE_INDEX(self):
		output = {'result':'success'}
	
		try:
			for poke_id in list(self.pdb.get_poke_all()):
				self.pdb.delete_poke(poke_id)
		except Exception as ex:
			output['result']='error'
			output['message']=str(ex)
	
		return json.dumps(output)
				
