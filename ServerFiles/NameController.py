import cherrypy
import re, json
from pokedex_library import _pokedex

class NameController(object):
	def __init__(self, pdb=None):
		if pdb is None:
			self.pdb = _pokedex()
			self.pdb.load_poke()
		else:
			self.pdb = pdb

	def GET_KEY(self, name):
		output = {'result':'success'}

		try:
			pokemon = self.pdb.get_poke_name(name)
			if pokemon[0] != 'error':
				output['name']=pokemon[0]
				output['type']=pokemon[1]
				output['hp']=int(pokemon[2])
				output['id']=int(pokemon[3])
			else:
				output['result']='error'
				output['message']='No pokemon found'
		except Exception as ex:
			output['result']='error'
			output['message']=str(ex)

		return json.dumps(output)
