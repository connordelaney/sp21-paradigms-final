// START UNUSED STUFF
/*function demo(){
    // if errors with API, use this for demo
    document.getElementById("pokemon-name").innerHTML = "Bulbasaur";
    makeNetworkCallToSprite(1);
}

function updatePagewithResponse(name, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("pokemon-name");

    if(response_json['result'] == 'error'){
        label1.innerHTML = 'Apologies, we could not find your name.'
    } else{
        label1.innerHTML =  response_json['name'];
        var id = parseInt(response_json['id']);
        makeNetworkCallToSprite(id);
    }
} // end of updateAgeWithResponse

function makeNetworkCallToSprite(id){
    //TODO
    console.log('entered make nw call ' + id);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://pokeapi.co/api/v2/pokemon/" + id;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        //console.log(xhr.responseText);
        // do something
        updateSpriteWithResponse(id, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null)

} // end of make nw call

function updateSpriteWithResponse(id, response_text){
    //TODO Change properties
    var response_json = JSON.parse(response_text);
    // update a label
    var sprite = document.getElementById("pri_image_url");

    if(response_json['result'] == 'error'){
        label1.innerHTML = 'Apologies, we could not find your name.'
    } else{
        var sprite_url = response_json['sprites']['front_default'];
        console.log('got new sprite url: ' + sprite_url);
        sprite.src = sprite_url;
        makeNetworkCallToSprite(id);
    }
} // end of updateSpriteWithResponse*/
// END UNUSED STUFF

// --------------------------- FROM PROFILE.JS --------------------------
/*function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var name = document.getElementById("search_text").value;
    console.log('Pokemon you entered is ' + name);
    makeNetworkCallToMyApi(name);

} // end of get form info

function makeNetworkCallToMyApi(name){
    console.log('entered make nw call to my api with name ' + name);
    // set up url

    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://student04.cse.nd.edu:51037/name/" + name;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr
    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updatePageWithResponse(name, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.log('error with call to my API')
        console.error(xhr.statusText);
        alert("Experienced an error with search API. Redirecting to home.");
        window.location.replace("index.html");
    }

    

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function demo(){
    // if errors with API, use this for demo
    document.getElementById("pokemon-name").innerHTML = "Bulbasaur";
    makeNetworkCallToSprite(1);
}

function updatePagewithResponse(name, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("pokemon-name");

    if(response_json['result'] == 'error'){
        label1.innerHTML = 'Apologies, we could not find your name.'
    } else{
        label1.innerHTML =  response_json['name'];
        var id = parseInt(response_json['id']);
        makeNetworkCallToSprite(id);
    }
} // end of updateAgeWithResponse*/

/*function getEvolutionChain(id){
    console.log('getting evolutions for id: ' + id);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://pokeapi.co/api/v2/evolution-chain/" + id;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log("got evolution chain");
        console.log("parsing JSON");
        var response_json = JSON.parse(xhr.responseText);
        var evolutions_div = document.getElementById("evolutions");
        
        evolution_chain = response_json["chain"];
        console.log("getting url");
        var evo_url = evolution_chain["species"]["url"];
        console.log("parsing id");
        var id = 1;
        var url_tokens = evo_url.split("/");
        if(url_tokens.length > 2){
            id = url_tokens[url_tokens.length-2];
        }
        console.log(id);
        getEvolutionImage(id);

        while(evolution_chain["evolves_to"].length > 0){
            evolution_chain = evolution_chain["evolves_to"][0];
            console.log("getting url");
            evo_url = evolution_chain["species"]["url"];
            console.log("parsing id");
            id = 1;
            url_tokens = evo_url.split("/");
            if(url_tokens.length > 2){
                id = url_tokens[url_tokens.length-2];
            }
            console.log(id);
            getEvolutionImage(id);
        }

        return response_json;
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null)
}

function getEvolutionImage(id){
    console.log('getting evolutions for id: ' + id);
    // set up url
    var url = "https://pokeapi.co/api/v2/pokemon/" + id;
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log("data for evolved pokemon");
        console.log("parsing JSON");
        var response_json = JSON.parse(xhr.responseText);
        var evolutions_div = document.getElementById("evolutions");

        console.log("creating image");
        var evolution_img = document.createElement("img");
        evolution_img.width = 100;
        evolution_img.height = 100;
        evolution_img.className = "media-object";
        evolution_img.src = response_json["sprites"]["front_default"]

        console.log("creating name");
        var evolution_name = document.createElement("h5");
        evolution_name.className = "media-heading";
        evolution_name.innerHTML = response_json["name"];

        evolutions_div.appendChild(evolution_img);
        evolutions_div.appendChild(evolution_name);
        return response_json;
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null)
}

function updateSpriteWithResponse(id, response_text){
    //TODO Change properties
    var response_json = JSON.parse(response_text);
    // update a label
    var sprite = document.getElementById("pri_image_url");

    if(response_json['result'] == 'error'){
        label1.innerHTML = 'Apologies, we could not find your name.'
    } else{
        var sprite_url = response_json['sprites']['front_default'];
        console.log('got new sprite url: ' + sprite_url);
        sprite.src = sprite_url;
        //var id = parseInt(response_json['id']);
        makeNetworkCallToSprite(id);
    }
} // end of updateSpriteWithResponse*/