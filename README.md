The Better Pokédex by Jake Feehery (jfeehery) and Connor Delaney (cdelane2)

Our website will be a virtual Pokédex - a collection of Pokémon and the information about them. Each Pokémon will have a webpage and you can navigate between them. Additionally, you can post comments and likes on any Pokémon.

The following table describes the API we'll use to exchange information about the Pokémon with the server.
We have implementation for PUT and POST but they are not used in the front end implementation and neither is the /type/ functionality, but they were fun to build and work with.

We used port number 51037, and we hope people can use our API to learn a little more about those funny little guys, the pokemon!

| Method | Resource| Input Body | Response Body | Description |
|:-------|:--------|:-----------|:--------------|:------------|
| GET    | /pokemon/ | none | {"result": "success", "entries": [{"name": "Bulbasaur", "type": ["Grass", "Poison"], "hp": 45, "id": 1}, {"name": "Ivysaur", "type": ["Grass", "Poison"], "hp": 60, "id": 2} ... } | Gives the list of all Pokémon in the Pokédex and their details|
| GET    | /pokemon/:id | none | {"result": "success", "name": "Venusaur", "type": ["Grass", "Poison"], "hp": 80, "id": 3} | Gives the details including posted likes and comments for some specific Pokémon|
|PUT|/pokemon/:id |{"name":"fake", "type":["Grass"], "hp", 100 }|{"result":"success"}| Replaces Pokemon at ID or creates new |
|POST|/pokemon/|{"name":"fake", "type":["Grass"], "hp", 100 }|{"result":"success", "id":152}| Creates a new pokemon and returns index|
|DELETE| /pokemon/:id |none|{"result":"success"}| Delete pokemon at ID |
|DELETE| /pokemon/ | none |{"result":"success"}| Delete all pokemon from the database |
|GET| /name/:name | none | {"result": "success", "name": "Bulbasaur", "type": ["Grass", "Poison"], "hp": 45, "id": 1} | Returns the first pokemon whose name begins with the search string
|GET| /type/:type | none | {"result": "success", "entries": [{"name": "Dratini", "type": ["Dragon"], "hp": 41, "id": 147}, {"name": "Dragonair", "type": ["Dragon"], "hp": 61, "id": 148}, {"name": "Dragonite", "type": ["Dragon", "Flying"], "hp": 91, "id": 149}]} | Returns all pokemons of a specified typing
|PUT| /reset/:id | none | {"result":"success"} | Resets a given pokemon with the specified ID 
| PUT| /reset/ | none | {"result":"success"} | Resets the entire pokedex back to the original state |

In order to run the server, log onto student04.cse.nd.edu, navigate into the ServerFiles folder and type "python3 server.py", this will run the server.

In order to use the front end, go to [this](http://connordelaney.gitlab.io/sp21-paradigms-final/) link and either navigate through the homepage or a search. Our database only contains the original 151 so the search bar only registers those pokemon. Clicking on the return to home button in the carousel will return you to the home page. We hope you have as much fun using our page as we did making it because we had a ball!
